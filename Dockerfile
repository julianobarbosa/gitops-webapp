FROM scratch

LABEL maintaner="Juliano Barbosa <julianomb@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
